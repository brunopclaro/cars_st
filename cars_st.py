#import libraries

import streamlit as st
import pandas as pd
import ml
import joblib


#Column names

cols=['MPG','Cylinders','Displacement','Horsepower','Weight','Acceleration','Model_Year','Origin']

# reading the .data file using pandas

df=pd.read_csv('/home/brunopclaro/flask_ml/auto-mpg.data',names=cols,na_values='?',comment = '\t',sep=' ',skipinitialspace=True)

#making copy of dataframe

data=df.copy()


data=data.drop(['MPG','Origin'],axis=1)





def get_user_input():
    Cylinders = st.sidebar.slider("Cylinders",3,8)
    Displacement = st.sidebar.slider("Displacement",68,455)
    Horsepower = st.sidebar.slider("Horsepower",46,230)
    Weight = st.sidebar.slider("Weight",1613,5140)
    Acceleration = st.sidebar.slider("Acceleration",8,25)
    Model_Year = st.sidebar.slider("Model_Year",70,82)

# Store dictionary into a variable

    user_data= {
        "Cylinders": Cylinders,
        "Displacement": Displacement,
        "Horsepower": Horsepower,
        "Weight": Weight,
        "Acceleration": Acceleration,
        "Model_Year": Model_Year,
    }

    features = pd.DataFrame(user_data, index=[0])
    return features

# Store users input into a variable

user_input=get_user_input()

data_app=data.append(user_input, ignore_index=True)

#Set subheader and display user input
st.subheader("Car user input")
st.write(user_input)

# Get prediction from model

with open('/home/brunopclaro/ml_test/model.pkl', 'rb') as f_in:
            model = joblib.load(f_in)

MPG = ml.predict_mpg(data_app,model)



#Set subheader and display MPG classification
st.subheader("MPG prediction")
st.write(MPG[-1])



