FROM python:3.7
COPY . /cars_st
WORKDIR /cars_st
RUN pip install -r requirements.txt
EXPOSE 8501
ENTRYPOINT ["streamlit","run"]
CMD ["cars_st.py"]
